package conforme.herrera.chavez.rodriguez.Activitys;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import conforme.herrera.chavez.rodriguez.R;

public class Detalle extends AppCompatActivity implements View.OnClickListener {
    Button fecha;
    private int dia,mes, año;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        fecha= (Button)findViewById(R.id.buttonfecha);
        fecha.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v==fecha){
            final Calendar c= Calendar.getInstance();
            dia= c.get(Calendar.DAY_OF_MONTH);
            mes=c.get(Calendar.MONTH);
            año=c.get(Calendar.YEAR);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    Toast toast = Toast.makeText(getApplication(), "cghhb", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

           ,dia,mes,año);
            datePickerDialog.show();

        }
    }
}
