package conforme.herrera.chavez.rodriguez.Activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import conforme.herrera.chavez.rodriguez.R;
import conforme.herrera.chavez.rodriguez.SqliteHelper;
import conforme.herrera.chavez.rodriguez.fragmentos.RegistroInicio;

public class Main2Activity extends  AppCompatActivity  {
    private TextView mTextMessage;

    ImageView f1;
    TextView textobuscador;

    private BottomNavigationView buttonNavigation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
      //  coneccion de la base de datos local
        SqliteHelper conn =new SqliteHelper(this, "db_cliente", null,1);


        f1 = (ImageView)findViewById(R.id.imageView2);

        f1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new
                        Intent(Main2Activity.this, Detalle.class);
                        startActivity(intent);
            }
        });

        buttonNavigation = (BottomNavigationView) findViewById(R.id.buttonNavigation);
        buttonNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                if (menuItem.getItemId() == R.id.busquedaItem) {


                } else if (menuItem.getItemId() == R.id.inicioItem) {
                    Intent intent = new Intent(Main2Activity.this, CHAT.class);
                    startActivity(intent);


    } else if (menuItem.getItemId() == R.id.sesionItem) {
                    Intent intent = new Intent(Main2Activity.this, RegistroInicio.class);
                    startActivity(intent);

                }
                return false;
            }
        });



    }
    }
