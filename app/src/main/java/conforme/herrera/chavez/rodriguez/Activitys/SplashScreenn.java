package conforme.herrera.chavez.rodriguez.Activitys;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import conforme.herrera.chavez.rodriguez.R;

public class SplashScreenn extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screenn);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent= new Intent(SplashScreenn.this, Main2Activity.class);
                startActivity(intent);
                finish();

            }
        },2000);

    }


}
