package conforme.herrera.chavez.rodriguez;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import conforme.herrera.chavez.rodriguez.R;

public class Adaptador extends BaseAdapter {
    //declaramos  variables necesarioas
    //nos sirve para instancuar el archivo xml creado en este caso elemento lista
    private static LayoutInflater inflater=null;
    //entorno de la aplicaion para que se genere el  adapador
    Context context;
    //matris de datos della acticidad principal
    String[][]datos;
    int[] datosImg;

    public Adaptador(Context context, String[][] datos, int[] imagen) {
        this.context = context;
        this.datos = datos;
        this.datosImg = imagen;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        final View view = inflater.inflate(R.layout.elementolista, null);
        TextView nombre= (TextView) view.findViewById(R.id.txtnombre);






        TextView descripcion=(TextView)view.findViewById(R.id.txtdescripcion);

        ImageView imagen =(ImageView)view.findViewById(R.id.imageView1);
        RatingBar calificacion = (RatingBar)view.findViewById(R.id.ratingBarM);

        nombre.setText(datos[i][0]);
        descripcion.setText(datos[i][1]);
        imagen.setImageResource(datosImg[i]);
        calificacion.setProgress(Integer.valueOf(datos[i][2]));

        return view;
    }

    @Override
    public int getCount() {
        return datosImg.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


}





