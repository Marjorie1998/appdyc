package conforme.herrera.chavez.rodriguez;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqliteHelper  extends SQLiteOpenHelper {

    final String CREAR_TABLA_CLIENTE=" CREATE TABLA Cliente( id Integer, String nombres, String apellido)";

    public SqliteHelper( Context context,    String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREAR_TABLA_CLIENTE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS USUARIO");
        onCreate(db);
    }
}
