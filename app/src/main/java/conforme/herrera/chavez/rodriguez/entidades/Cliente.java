package conforme.herrera.chavez.rodriguez.entidades;

import android.provider.ContactsContract;
import android.text.LoginFilter;

import java.security.KeyStore;

public class Cliente {

    private Integer id;
    private String nombres;
    private  String apellido;
    private ContactsContract.CommonDataKinds.Email correo;
    private KeyStore.PasswordProtection contrsena;

    public Cliente(Integer id, String nombres, String apellido, ContactsContract.CommonDataKinds.Email correo, KeyStore.PasswordProtection contrsena) {
        this.id = id;
        this.nombres = nombres;
        this.apellido = apellido;
        this.correo = correo;
        this.contrsena = contrsena;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public ContactsContract.CommonDataKinds.Email getCorreo() {
        return correo;
    }

    public void setCorreo(ContactsContract.CommonDataKinds.Email correo) {
        this.correo = correo;
    }

    public KeyStore.PasswordProtection getContrsena() {
        return contrsena;
    }

    public void setContrsena(KeyStore.PasswordProtection contrsena) {
        this.contrsena = contrsena;
    }
}
