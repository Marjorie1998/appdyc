package conforme.herrera.chavez.rodriguez.fragmentos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import conforme.herrera.chavez.rodriguez.Activitys.Main2Activity;
import conforme.herrera.chavez.rodriguez.R;


public class Registro extends Fragment {
Button  registrarCliente;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_registro, container, false);
        // Inflate the layout for this fragment

        registrarCliente =(Button)view.findViewById(R.id.buttonIS);
         registrarCliente.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent;
                 intent = new Intent(getActivity(), InicioSesion.class);
                 startActivity(intent);

             }
         });


        return  view;
    }


  }
